const express = require("express");
const app = express();

app.get("/", function (req, res) {
    res.send("HelloWorld, this was released at 2020/10/22 14:25");
});

module.exports = {
    path: "/api/",
    handler: app
};