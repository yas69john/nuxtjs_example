#!/bin/bash -e
source ~/.bash_profile

echo 'ApplicationStart shell started.'
sudo pm2 start npm -- start
sudo pm2 save
